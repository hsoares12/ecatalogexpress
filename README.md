 # eCatalog Express

## Getting started
Project built on Ubuntu. Go into projet root (ecatalogexpress). 

!!! Message en français très important !!!
Si un utilisateur est connecté au lancement de l'application (il sera écrit logout en haut à droite de l'interface utilisateur), veuillez vous déconnecter. Ensuite, vous pouvez faire ce que vous voulez.

### Instanciate containers

``` bash
sudo docker-compose -d
```

### Build containers

``` bash
sudo docker-compose --build
```

### If needed, send SQL script into database to initialize

``` bash
sudo docker exec -i ecatalogexpress_db_1 mysql -p2019 < bd_init/init.sql
```

### Connect to the bash into the running MySQL container
``` bash
sudo docker exec -t -i ecatalogexpress_db_1 /bin/bash
```

### Run MySQL client from bash MySQL container
``` bash
mysql -uroot -p2019
```

### Use ecatalogexpress database, when running MySQL client
``` bash
use ecatalogexpress;
```
