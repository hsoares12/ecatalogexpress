DROP DATABASE IF EXISTS ecatalogexpress;
CREATE DATABASE IF NOT EXISTS ecatalogexpress;
USE ecatalogexpress;

DROP TABLE IF EXISTS users;

CREATE TABLE IF NOT EXISTS users (
    id_user INT UNSIGNED NOT NULL AUTO_INCREMENT,
    username VARCHAR(20) NOT NULL UNIQUE,
    email VARCHAR(120) NOT NULL UNIQUE,
    password VARCHAR(60) NOT NULL,
    PRIMARY KEY (id_user)
);

DROP TABLE IF EXISTS products;
 
CREATE TABLE IF NOT EXISTS products ( 
    id_product  INT UNSIGNED NOT NULL AUTO_INCREMENT,
    price       FLOAT        NOT NULL,
    description VARCHAR(150) NOT NULL,
    name        VARCHAR(40)  NOT NULL,
    category    VARCHAR(40)  NOT NULL,
    image       TEXT         NOT NULL,
    quantity    INT UNSIGNED NOT NULL,
    PRIMARY KEY (id_product)
);

DROP TABLE IF EXISTS carts;
 
CREATE TABLE IF NOT EXISTS carts (
    id_user INT UNSIGNED NOT NULL,
    id_product  INT UNSIGNED NOT NULL,
    quantity_in_cart INT UNSIGNED NOT NULL,
    PRIMARY KEY (id_user, id_product),
    FOREIGN KEY (id_user) REFERENCES users(id_user) ON UPDATE CASCADE, 
    FOREIGN KEY (id_product) REFERENCES products(id_product) ON UPDATE CASCADE
);


CREATE INDEX user_cart ON carts (id_user)
USING HASH;

CREATE INDEX product_id ON products (id_product)
USING HASH;

CREATE INDEX product_desc ON products (description)
USING HASH;

    
DELIMITER //
CREATE TRIGGER product_quantity_check_when_inserting_in_cart BEFORE INSERT ON carts
FOR EACH ROW
BEGIN
    DECLARE quantity INT UNSIGNED;
    SELECT products.quantity
    INTO quantity
    FROM products
    WHERE products.id_product = NEW.id_product;
    
    IF quantity >= NEW.quantity_in_cart THEN
        UPDATE products 
        SET products.quantity = products.quantity - NEW.quantity_in_cart
        WHERE products.id_product = NEW.id_product;
    ELSE    
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'Not enough products left in stock after insert on carts.';
    END IF;
END //
DELIMITER ;


DELIMITER //
CREATE TRIGGER product_quantity_check_when_updating_cart BEFORE UPDATE ON carts
FOR EACH ROW
BEGIN
    DECLARE quantity INT UNSIGNED;
    SELECT products.quantity
    INTO quantity
    FROM products
    WHERE products.id_product = NEW.id_product;
    
    IF quantity >= NEW.quantity_in_cart - OLD.quantity_in_cart THEN
        UPDATE products 
        SET products.quantity = products.quantity - (NEW.quantity_in_cart - OLD.quantity_in_cart)
        WHERE products.id_product = NEW.id_product;
    ELSE    
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'Not enough products left in stock after update on carts.';
    END IF;
END //
DELIMITER ; 


DELIMITER //
CREATE TRIGGER product_quantity_update_when_delete_on_cart AFTER DELETE ON carts
FOR EACH ROW
BEGIN
    DECLARE quantity INT UNSIGNED;
    SELECT products.quantity
    INTO quantity
    FROM products
    WHERE products.id_product = OLD.id_product;
    
    UPDATE products 
    SET products.quantity = products.quantity + OLD.quantity_in_cart
    WHERE products.id_product = OLD.id_product;
END //
DELIMITER ; 



INSERT INTO products (price, description, name, category, image, quantity) VALUES (69913, 'at turpis donec posuere metus vitae ipsum aliquam non mauris.', 'Uraeginthus granatina', 'Animals', 'https://loremflickr.com/320/240/animals', 1);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (22237, 'eget tempus vel pede morbi porttitor.', 'Varanus komodensis', 'Arts', 'https://loremflickr.com/320/240/arts', 9);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (132860, 'penatibus et magnis dis parturient montes.', 'Hippopotamus amphibius', 'Arts', 'https://loremflickr.com/320/240/arts', 8);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (168157, 'id lobortis convallis.', 'Sarkidornis melanotos', 'Arts', 'https://loremflickr.com/320/240/arts', 7);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (174414, 'elementum nullam varius nulla facilisi cras non.', 'Phalaropus lobatus', 'Music', 'https://loremflickr.com/320/240/music', 5);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (123481, 'sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus.', 'Lamprotornis nitens', 'Kitchen', 'https://loremflickr.com/320/240/kitchen', 8);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (145681, 'non interdum in ante.', 'Otocyon megalotis', 'Cars', 'https://loremflickr.com/320/240/cars', 6);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (154099, 'ornare imperdiet sapien urna pretium.', 'Dicrurus adsimilis', 'Cars', 'https://loremflickr.com/320/240/cars', 3);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (26071, 'lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar.', 'Spizaetus coronatus', 'Arts', 'https://loremflickr.com/320/240/arts', 5);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (103851, 'imperdiet sapien urna.', 'Diomedea irrorata', 'House', 'https://loremflickr.com/320/240/house', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (150605, 'risus praesent lectus vestibulum quam sapien varius ut blandit non.', 'Castor fiber', 'Toys', 'https://loremflickr.com/320/240/toys', 5);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (123130, 'elementum nullam varius nulla facilisi cras non velit nec.', 'Chlidonias leucopterus', 'Animals', 'https://loremflickr.com/320/240/animals', 3);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (97243, 'platea dictumst maecenas ut.', 'Phalaropus fulicarius', 'Books', 'https://loremflickr.com/320/240/books', 1);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (198111, 'scelerisque mauris sit amet.', 'Canis latrans', 'Costumes', 'https://loremflickr.com/320/240/costumes', 7);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (22881, 'bibendum imperdiet nullam orci pede venenatis non sodales.', 'Lama pacos', 'Books', 'https://loremflickr.com/320/240/books', 9);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (187216, 'donec vitae nisi nam ultrices libero non mattis.', 'Procyon lotor', 'School', 'https://loremflickr.com/320/240/school', 6);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (100391, 'dapibus duis at.', 'Fratercula corniculata', 'Costumes', 'https://loremflickr.com/320/240/costumes', 1);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (29020, 'nullam orci pede venenatis non sodales sed.', 'Spermophilus parryii', 'Gaming', 'https://loremflickr.com/320/240/gaming', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (123149, 'varius ut blandit non interdum.', 'Rhea americana', 'Books', 'https://loremflickr.com/320/240/books', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (50974, 'est congue elementum in hac habitasse platea dictumst morbi vestibulum.', 'Felis concolor', 'Kitchen', 'https://loremflickr.com/320/240/kitchen', 10);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (153184, 'augue vestibulum rutrum rutrum neque.', 'Balearica pavonina', 'Business', 'https://loremflickr.com/320/240/business', 1);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (32044, 'habitasse platea dictumst morbi vestibulum velit id pretium.', 'Cereopsis novaehollandiae', 'Health', 'https://loremflickr.com/320/240/health', 1);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (85133, 'lacus morbi sem mauris laoreet ut rhoncus.', 'Myrmecophaga tridactyla', 'Toys', 'https://loremflickr.com/320/240/toys', 6);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (45654, 'cubilia curae nulla dapibus dolor vel est.', 'Zalophus californicus', 'Music', 'https://loremflickr.com/320/240/music', 10);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (118566, 'nibh in hac habitasse platea dictumst aliquam.', 'Sarkidornis melanotos', 'Arts', 'https://loremflickr.com/320/240/arts', 3);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (179957, 'fusce posuere felis.', 'Eudromia elegandwq', 'Health', 'https://loremflickr.com/320/240/health', 2);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (128115, 'sem fusce consequat nulla nisl nunc.', 'Amblyrhynchus cristatus', 'Sports', 'https://loremflickr.com/320/240/sports', 2);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (38732, 'sed vestibulum sit.', 'Eudromia elegans', 'School', 'https://loremflickr.com/320/240/school', 2);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (131820, 'vestibulum sed magna at nunc commodo placerat praesent.', 'Phoeniconaias minor', 'Movies', 'https://loremflickr.com/320/240/movies', 8);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (47243, 'sem mauris laoreet ut rhoncus aliquet pulvinar.', 'Dasypus novemcinctus', 'Food', 'https://loremflickr.com/320/240/food', 7);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (107539, 'interdum in ante.', 'Motacilla aguimp', 'Books', 'https://loremflickr.com/320/240/books', 7);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (54449, 'nisl venenatis lacinia aenean sit amet.', 'Bugeranus caruncalatus', 'Health', 'https://loremflickr.com/320/240/health', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (46383, 'habitasse platea dictumst maecenas ut massa.', 'Felis silvestris lybica', 'Fashion', 'https://loremflickr.com/320/240/fashion', 9);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (35636, 'ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel.', 'Cebus apella', 'Toys', 'https://loremflickr.com/320/240/toys', 2);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (160027, 'ante ipsum primis in.', 'Microcebus murinus', 'Sports', 'https://loremflickr.com/320/240/sports', 2);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (175492, 'dictumst maecenas ut massa.', 'Macropus eugenii', 'Cars', 'https://loremflickr.com/320/240/cars', 6);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (144769, 'elementum eu interdum eu tincidunt.', 'Uraeginthus granatina', 'Cars', 'https://loremflickr.com/320/240/cars', 7);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (50197, 'dui proin leo.', 'Laniarius ferrugineus', 'Arts', 'https://loremflickr.com/320/240/arts', 2);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (166363, 'praesent blandit lacinia.', 'Anastomus oscitans', 'Food', 'https://loremflickr.com/320/240/food', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (64596, 'cursus urna ut tellus nulla.', 'Streptopelia decipiens', 'Sports', 'https://loremflickr.com/320/240/sports', 3);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (34155, 'pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis.', 'Ictalurus furcatus', 'Kitchen', 'https://loremflickr.com/320/240/kitchen', 6);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (78221, 'amet justo morbi ut odio cras mi pede.', 'mustafa lidor', 'Toys', 'https://loremflickr.com/320/240/toys', 10);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (154315, 'arcu sed augue.', 'octopus Hippopotamus', 'Kitchen', 'https://loremflickr.com/320/240/kitchen', 6);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (117225, 'id turpis integer aliquet massa id lobortis.', 'Neophron percnopterus', 'Arts', 'https://loremflickr.com/320/240/arts', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (62765, 'odio justo sollicitudin ut suscipit.', 'Odocoileus hemionus', 'School', 'https://loremflickr.com/320/240/school', 6);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (31731, 'adipiscing elit proin risus praesent lectus vestibulum quam sapien varius.', 'Mazama americana', 'Music', 'https://loremflickr.com/320/240/music', 9);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (178538, 'sed augue aliquam erat volutpat in congue etiam.', 'Lorythaixoides concolor', 'Gaming', 'https://loremflickr.com/320/240/gaming', 2);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (119635, 'mauris non ligula pellentesque ultrices phasellus id sapien.', 'Tyto novaehollandiae', 'Handmade', 'https://loremflickr.com/320/240/handmade', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (28347, 'lorem id ligula suspendisse ornare consequat lectus in est risus.', 'Pseudocheirus peregrinus', 'Books', 'https://loremflickr.com/320/240/books', 6);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (22876, 'donec dapibus duis.', 'maneger dwqdqwpdw', 'Food', 'https://loremflickr.com/320/240/food', 9);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (93808, 'sed vel enim sit amet nunc viverra dapibus nulla.', 'Callipepla gambelii', 'Food', 'https://loremflickr.com/320/240/food', 10);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (121429, 'eget orci vehicula condimentum curabitur in libero ut massa volutpat.', 'Ninox superciliaris', 'Cars', 'https://loremflickr.com/320/240/cars', 9);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (30800, 'facilisi cras non velit nec nisi vulputate nonummy maecenas.', 'Macropus rufogriseus', 'School', 'https://loremflickr.com/320/240/school', 9);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (187199, 'massa id lobortis convallis tortor risus dapibus augue vel accumsan.', 'Acrantophis madagascariensis', 'Sports', 'https://loremflickr.com/320/240/sports', 7);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (29026, 'consequat dui nec nisi.', 'Felis serval', 'Garden', 'https://loremflickr.com/320/240/garden', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (40530, 'enim in tempor turpis nec euismod scelerisque quam turpis adipiscing.', 'Axis axis', 'Business', 'https://loremflickr.com/320/240/business', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (35802, 'ut massa quis augue luctus tincidunt nulla.', 'Cercatetus concinnus', 'Garden', 'https://loremflickr.com/320/240/garden', 3);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (24958, 'phasellus in felis.', 'Chlidonias leucopterus', 'Health', 'https://loremflickr.com/320/240/health', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (101222, 'porta volutpat erat.', 'Taxidea taxus', 'Animals', 'https://loremflickr.com/320/240/animals', 9);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (61030, 'neque libero convallis eget eleifend luctus ultricies eu.', 'Oryx gazella callotis', 'Business', 'https://loremflickr.com/320/240/business', 2);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (59041, 'sapien sapien non mi integer ac.', 'Heloderma horridum', 'Garden', 'https://loremflickr.com/320/240/garden', 6);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (136195, 'lacus at turpis donec posuere metus vitae ipsum aliquam.', 'Alopochen aegyptiacus', 'Fashion', 'https://loremflickr.com/320/240/fashion', 2);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (118975, 'id massa id nisl venenatis lacinia.', 'Sterna paradisaea', 'Toys', 'https://loremflickr.com/320/240/toys', 8);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (32524, 'consequat varius integer ac leo pellentesque.', 'Dicrostonyx groenlandicus', 'Costumes', 'https://loremflickr.com/320/240/costumes', 1);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (60592, 'eget tempus vel pede morbi.', 'Corallus hortulanus cooki', 'Handmade', 'https://loremflickr.com/320/240/handmade', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (118405, 'massa quis augue luctus tincidunt nulla.', 'Platalea leucordia', 'Garden', 'https://loremflickr.com/320/240/garden', 8);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (96180, 'eu interdum eu tincidunt.', 'Bassariscus astutus', 'Kitchen', 'https://loremflickr.com/320/240/kitchen', 2);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (185779, 'et ultrices posuere cubilia curae nulla dapibus dolor vel.', 'Pavo cristatus', 'Fashion', 'https://loremflickr.com/320/240/fashion', 8);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (159338, 'lectus aliquam sit.', 'Hystrix cristata', 'School', 'https://loremflickr.com/320/240/school', 1);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (125678, 'varius integer ac leo pellentesque ultrices mattis.', 'Capra ibex', 'Business', 'https://loremflickr.com/320/240/business', 5);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (85183, 'sed augue aliquam erat.', 'Cabassous sp', 'Books', 'https://loremflickr.com/320/240/books', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (158938, 'ornare consequat lectus in est risus auctor.', 'Physignathus cocincinus', 'Sports', 'https://loremflickr.com/320/240/sports', 1);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (183345, 'risus semper porta volutpat quam pede lobortis ligula.', 'Gymnorhina tibicen', 'Garden', 'https://loremflickr.com/320/240/garden', 6);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (186640, 'vel dapibus at diam nam tristique tortor eu.', 'Cervus duvauceli', 'Business', 'https://loremflickr.com/320/240/business', 6);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (21083, 'libero quis orci nullam molestie.', 'Alouatta seniculus', 'Arts', 'https://loremflickr.com/320/240/arts', 3);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (139482, 'libero convallis eget eleifend luctus ultricies eu nibh quisque.', 'Estrilda erythronotos', 'Sports', 'https://loremflickr.com/320/240/sports', 9);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (75828, 'nascetur ridiculus mus vivamus vestibulum sagittis sapien.', 'Phoeniconaias minor', 'Handmade', 'https://loremflickr.com/320/240/handmade', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (55355, 'a ipsum integer a.', 'Elephas maximus bengalensis', 'Handmade', 'https://loremflickr.com/320/240/handmade', 1);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (118408, 'vivamus vel nulla eget eros elementum pellentesque quisque porta.', 'Chelodina longicollis', 'Kitchen', 'https://loremflickr.com/320/240/kitchen', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (119450, 'hac habitasse platea dictumst morbi vestibulum velit.', 'Ephippiorhynchus mycteria', 'House', 'https://loremflickr.com/320/240/house', 7);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (82021, 'nulla nunc purus phasellus.', 'Balearica pavonina', 'House', 'https://loremflickr.com/320/240/house', 5);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (182084, 'in quis justo maecenas rhoncus aliquam lacus morbi quis.', 'Tetracerus quadricornis', 'Health', 'https://loremflickr.com/320/240/health', 8);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (164158, 'molestie lorem quisque ut erat curabitur gravida.', 'Centrocercus urophasianus', 'Costumes', 'https://loremflickr.com/320/240/costumes', 7);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (95577, 'vestibulum sed magna at nunc commodo placerat praesent blandit.', 'Phoenicopterus ruber', 'Kitchen', 'https://loremflickr.com/320/240/kitchen', 2);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (134970, 'luctus et ultrices posuere cubilia curae.', 'Hystrix cristata', 'Health', 'https://loremflickr.com/320/240/health', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (189861, 'interdum mauris ullamcorper purus sit amet nulla quisque.', 'Mirounga angustirostris', 'Toys', 'https://loremflickr.com/320/240/toys', 5);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (74867, 'lacus morbi quis tortor id nulla ultrices aliquet.', 'Macropus rufus', 'Animals', 'https://loremflickr.com/320/240/animals', 6);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (49078, 'eu est congue.', 'Pseudocheirus peregrinus', 'Health', 'https://loremflickr.com/320/240/health', 2);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (69573, 'eu tincidunt in leo maecenas.', 'Ciconia episcopus', 'Arts', 'https://loremflickr.com/320/240/arts', 9);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (147224, 'euismod scelerisque quam turpis adipiscing.', 'Marmota monax', 'Arts', 'https://loremflickr.com/320/240/arts', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (135401, 'ipsum dolor sit amet consectetuer adipiscing elit proin risus.', 'Cathartes aura', 'Cars', 'https://loremflickr.com/320/240/cars', 2);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (113366, 'ac leo pellentesque ultrices mattis odio donec vitae.', 'Stenella coeruleoalba', 'Health', 'https://loremflickr.com/320/240/health', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (104587, 'ut massa quis augue luctus tincidunt.', 'Theropithecus gelada', 'Gaming', 'https://loremflickr.com/320/240/gaming', 1);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (152671, 'eu orci mauris lacinia sapien quis libero nullam sit amet.', 'Larus sp', 'Books', 'https://loremflickr.com/320/240/books', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (163708, 'vestibulum sagittis sapien cum sociis natoque penatibus.', 'Macropus eugenii', 'Gaming', 'https://loremflickr.com/320/240/gaming', 7);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (105938, 'ultrices posuere cubilia curae mauris viverra diam vitae.', 'mistafa moleke', 'Animals', 'https://loremflickr.com/320/240/animals', 8);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (129303, 'cum sociis natoque penatibus et magnis dis parturient montes nascetur.', 'Panthera leo persica', 'House', 'https://loremflickr.com/320/240/house', 6);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (155879, 'turpis a pede posuere nonummy integer non.', 'Semnopithecus entellus', 'Health', 'https://loremflickr.com/320/240/health', 7);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (144216, 'sed vestibulum sit amet cursus id turpis integer aliquet.', 'Haematopus ater', 'Garden', 'https://loremflickr.com/320/240/garden', 9);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (51893, 'non mi integer ac neque duis bibendum morbi non quam.', 'Perameles nasuta', 'Books', 'https://loremflickr.com/320/240/books', 6);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (142431, 'ut nunc vestibulum ante ipsum.', 'Phalaropus lobatus', 'Toys', 'https://loremflickr.com/320/240/toys', 9);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (183801, 'eros viverra eget.', 'Macropus eugenii', 'Handmade', 'https://loremflickr.com/320/240/handmade', 8);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (126577, 'augue vestibulum rutrum.', 'Spizaetus coronatus', 'Cars', 'https://loremflickr.com/320/240/cars', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (124657, 'pretium iaculis diam erat fermentum justo nec condimentum.', 'Falco peregrinus', 'Fashion', 'https://loremflickr.com/320/240/fashion', 2);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (83236, 'justo lacinia eget.', 'Sarkidornis melanotos', 'Fashion', 'https://loremflickr.com/320/240/fashion', 6);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (105308, 'eu sapien cursus vestibulum proin eu mi nulla ac.', 'Damaliscus lunatus', 'Movies', 'https://loremflickr.com/320/240/movies', 8);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (34872, 'morbi porttitor lorem id ligula suspendisse.', 'Paraxerus cepapi', 'Food', 'https://loremflickr.com/320/240/food', 10);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (140169, 'sem duis aliquam convallis nunc proin at turpis a pede.', 'Megaderma spasma', 'Animals', 'https://loremflickr.com/320/240/animals', 10);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (22388, 'non pretium quis lectus suspendisse potenti in eleifend quam a.', 'Axis axis', 'Toys', 'https://loremflickr.com/320/240/toys', 3);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (52372, 'morbi sem mauris laoreet ut rhoncus.', 'Myiarchus tuberculifer', 'Music', 'https://loremflickr.com/320/240/music', 1);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (58423, 'rutrum ac lobortis.', 'Casmerodius albus', 'Toys', 'https://loremflickr.com/320/240/toys', 6);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (46970, 'lorem quisque ut erat curabitur gravida nisi.', 'oluspectus', 'Health', 'https://loremflickr.com/320/240/health', 2);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (177605, 'eu mi nulla.', 'Pelecanus conspicillatus', 'Toys', 'https://loremflickr.com/320/240/toys', 8);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (110987, 'in congue etiam justo etiam pretium iaculis.', 'grjsq gravida', 'Gaming', 'https://loremflickr.com/320/240/gaming', 5);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (69922, 'nulla neque libero convallis eget eleifend.', 'Branta canadensis', 'Toys', 'https://loremflickr.com/320/240/toys', 8);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (92760, 'nascetur ridiculus mus.', 'Calyptorhynchus magnificus', 'Animals', 'https://loremflickr.com/320/240/animals', 4);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (109990, 'at dolor quis odio consequat varius integer ac.', 'Ciconia episcopus', 'Handmade', 'https://loremflickr.com/320/240/handmade', 10);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (186272, 'ac tellus semper.', 'Macropus rufus', 'Arts', 'https://loremflickr.com/320/240/arts', 2);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (58084, 'amet justo morbi.', 'Camelus dromedarius', 'Handmade', 'https://loremflickr.com/320/240/handmade', 3);
INSERT INTO products (price, description, name, category, image, quantity) VALUES (73812, 'congue etiam justo etiam pretium iaculis justo in hac habitasse.', 'Bugeranus caruncalatus', 'Health', 'https://loremflickr.com/320/240/health', 2);