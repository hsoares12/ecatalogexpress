from flask import Flask, render_template, url_for, flash, redirect, session
from flask_bcrypt import Bcrypt
from forms import RegistrationForm, LoginForm, AddToMyCartForm
from functools import wraps
import mysql.connector


application = Flask(__name__)
application.config['SECRET_KEY'] = '6e3c27a8a1bc7d2f5c419f9cd06aa149eaaa20e1'
bcrypt = Bcrypt(application)


config = {
        'user': 'root',
        'password': '2019',
        'host': 'db',
        'port': '3306',
		'database': 'ecatalogexpress'
}


# Call this method to connect to database
def connect_to_mysql_db():
    connection = mysql.connector.connect(**config)
    return connection

# Login decorator, if you use it, you need to be logged in to access the page
def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session and session['logged_in']:
            return f(*args, **kwargs)
        else:
            flash("You're not logged in!", 'danger')
            return redirect(url_for('login'))

    return wrap


# Home page
@application.route("/")
@application.route("/home")
def home():
    return render_template("home.html")


# About page
@application.route("/about")
def about():
    return render_template("about.html", title='About')     


@application.route("/sign-up", methods=['GET', 'POST'])
def sign_up():
    if 'logged_in' in session and session['logged_in']:
        flash('Logout to create a new account!', category='danger')
        return redirect(url_for('home'))

    # Execute when user submit form
    form = RegistrationForm()
    if form.validate_on_submit():
        
        # Hashing algorithm
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')

        connection = connect_to_mysql_db()
        cursor = connection.cursor()

        # Sign up user query here
        add_user_query = ("INSERT INTO users"
                          "(username, email, password) "
                          "VALUES (%s, %s, %s)")

        data_user = (form.username.data, form.email.data, hashed_password)

        cursor.execute(add_user_query, data_user)
        connection.commit()
        cursor.close()
        connection.close()

        flash('Account created. You can log in now!', 'success')
        return redirect(url_for('login'))

    return render_template('sign-up.html', title='Sign up', form=form)


@application.route("/login", methods=['GET', 'POST'])
def login():
    if 'logged_in' in session and session['logged_in']:
        flash('Log out to login!', category='danger')
        return redirect(url_for('home'))


    # Execute when user submit form
    form = LoginForm()
    if form.validate_on_submit():

        # Check if user is in table users
        connection = connect_to_mysql_db()
        cursor = connection.cursor()
        check_for_user_with_same_email = ("SELECT * FROM users WHERE email = %s")
        data_email = form.email.data
        cursor.execute(check_for_user_with_same_email, (data_email,))
        user = cursor.fetchone()
        connection.commit()
        cursor.close()
        connection.close()

        # If so, log him in
        if user and bcrypt.check_password_hash(user[3], form.password.data):
            session['id'] = user[0]
            session['username'] = user[1]
            session['email'] = user[2]
            session['logged_in'] = True
            return redirect(url_for('account'))
        else:
            flash('Login unsuccessful. Please check username and password.', 'danger')
    return render_template('login.html', title='Login', form=form)


@application.route("/logout")
@login_required
def logout():

    # Logout when clicking on logout on top bar
    session.pop('username', None)
    session['logged_in'] = False
    return redirect(url_for('home'))


@application.route("/account")
@login_required
def account():
    
    # Get all products related to the user in progress
    id_user_in_progress = session['id']
    products_in_cart = []
    connection = connect_to_mysql_db()
    cursor = connection.cursor()
    get_user_products_in_cart = ("SELECT products.id_product, products.name, products.category, products.price, carts.quantity_in_cart FROM products INNER JOIN carts ON products.id_product = carts.id_product WHERE carts.id_user = %s")
    cursor.execute(get_user_products_in_cart, (id_user_in_progress,))
    data = cursor.fetchall()
    connection.commit()
    cursor.close()
    connection.close()

    for row in data:
        products_in_cart.append({
            'id_product' : row[0],
            'name' : row[1],
            'category' : row[2],
            'price' : row[3],
            'quantity_in_cart' : row[4],
            'subtotal_price' : (row[4]*row[3])
        })

    total = 0
    for product in products_in_cart:
        total = total + product['subtotal_price']

    return render_template('account.html', title='Account', products_in_cart=products_in_cart, total=total)


@application.route("/categories", methods=['GET', 'POST'])
def categories():
    
    # Get all distincts categories
    categories = []
    connection = connect_to_mysql_db()
    cursor = connection.cursor(buffered=True)
    
    fetch_categories = ("SELECT DISTINCT category FROM products")
    cursor.execute(fetch_categories)
    rows = cursor.fetchall()
    connection.commit()
    cursor.close()
    connection.close()

    for row in rows:
        categories.append(row[0])
    
    return render_template('categories.html', title='Categories', categories=categories)


@application.route('/categories/<string:category>', methods=['GET', 'POST'])
def get_products_by_category(category):
    
    # Get list a products for a specific category
    products = []
    connection = connect_to_mysql_db()
    cursor = connection.cursor(buffered=True)
    fetch_products_by_category = ("SELECT * FROM products WHERE category = %s")
    cursor.execute(fetch_products_by_category, (category,))
    rows = cursor.fetchall()
    connection.commit()
    cursor.close()
    connection.close()


    for row in rows:
        products.append({
            'id_product' : row[0],
            'price' : row[1],
            'description' : row[2],
            'name' : row[3],
            'category' : row[4],
            'image' : row[5]
        })

    page_title = 'Products : ' + str(category).capitalize()

    return render_template('products-by-category.html', title=page_title, 
                            products=products, category=category)


@application.route('/categories/<string:category>/products/<int:id_product>', methods=['GET', 'POST'])
def products(category, id_product):
    
    # Display detailed informations for products
    connection = connect_to_mysql_db()
    cursor = connection.cursor(buffered=True)
    fetch_product_by_id_product = ("SELECT * FROM products WHERE category = %s AND id_product = %s")
    cursor.execute(fetch_product_by_id_product, (category, id_product))
    product_data = cursor.fetchone()
    connection.commit()
    cursor.close()
    connection.close

    product = {
        'id_product' : product_data[0],
        'price' : product_data[1],
        'description' : product_data[2],
        'name' : product_data[3],
        'category' : product_data[4],
        'image' : product_data[5],
        'quantity' : product_data[6]
    }

    # When user clicks on Add to my cart button
    form = AddToMyCartForm()
    if form.validate_on_submit():
        if 'logged_in' in session and session['logged_in']:
            
            
            # Check if product is avaible for the quantity desired by user 
            connection = connect_to_mysql_db()
            cursor = connection.cursor()
            get_product_quantity_in_page = ("SELECT quantity FROM products WHERE id_product = %s")
            cursor.execute(get_product_quantity_in_page, (id_product,))
            response = cursor.fetchone()
            quantity_of_the_product = { 'quantity' : response[0] }
            connection.commit()
            cursor.close()
            connection.close()

            if quantity_of_the_product['quantity'] < form.quantity_desired.data:
                flash('Not enough products left. Please choose less!', 'danger')

            # Looking if we have already the desired product in the cart of the user
            connection = connect_to_mysql_db()

            cursor = connection.cursor()
            get_user_products_in_cart = ("SELECT products.id_product, carts.quantity_in_cart FROM products INNER JOIN carts ON products.id_product = carts.id_product WHERE carts.id_user = %s AND carts.id_product = %s")
            id_user_in_progress = session['id']
            cursor.execute(get_user_products_in_cart, (id_user_in_progress, id_product))
            response = cursor.fetchone()
            connection.commit()
            cursor.close()
            connection.close()

            # If user has already the product, we only change his quantity
            if response:
                connection = connect_to_mysql_db()

                cursor = connection.cursor()
                update_cart_user = ("UPDATE carts SET quantity_in_cart = %s WHERE id_user = %s AND id_product = %s")
                quantity_actually_in_cart = response[1] 
                quantity_in_cart_updated = quantity_actually_in_cart + form.quantity_desired.data
                data_for_update = (quantity_in_cart_updated, session['id'], id_product)
                cursor.execute(update_cart_user, data_for_update)
                connection.commit()
                cursor.close()
                connection.close()
                return redirect(url_for('account'))

            # If user hasn't already the product, add the product to his cart
            else:
                connection = connect_to_mysql_db()
                cursor = connection.cursor()

                add_product_to_cart = ("INSERT INTO carts"
                              "(id_user, id_product, quantity_in_cart) "
                              "VALUES (%s, %s, %s)")

                data_cart =  (session["id"], id_product, form.quantity_desired.data)
                cursor.execute(add_product_to_cart, data_cart)
                connection.commit()
                cursor.close()
                connection.close()
                return redirect(url_for('account'))


    return render_template('product.html', title=product['name'].capitalize(), product=product, 
                            category=category, form=form)


@application.route('/delete_product/<int:id_product>', methods=['GET', 'POST'])
@login_required
def delete_product(id_product):

    # Delete product from cart
    connection = connect_to_mysql_db()
    cursor = connection.cursor()
    delete_product_query = ("DELETE FROM carts WHERE id_user = %s AND id_product = %s")
    cursor.execute(delete_product_query, (session['id'], id_product))
    connection.commit()
    cursor.close()
    connection.close()
    return redirect(url_for('account'))



if __name__ == '__main__':
    application.run(host='0.0.0.0')