from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, IntegerField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError, NumberRange


class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired(),Length(min=4, max=60)])
    confirm_password = PasswordField('Confirm password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Sign up')


    def validate_username(self, username):
        connection = connect_to_mysql_db()

        cursor = connection.cursor()

        check_for_user_with_same_username = ("SELECT * FROM users WHERE username = (%s)")
        data_username = username.data
        cursor.execute(check_for_user_with_same_username, (data_username,))
        user = cursor.fetchone()

        connection.commit()
        cursor.close()
        connection.close()

        if user:
            raise ValidationError('That username is already owned by someone. Please choose a different one!')


    def validate_email(self, email):
        connection = connect_to_mysql_db()

        cursor = connection.cursor()

        check_for_user_with_same_email = ("SELECT * FROM users WHERE email = %s")
        data_email = email.data
        cursor.execute(check_for_user_with_same_email, (data_email,))
        user = cursor.fetchone()

        connection.commit()
        cursor.close()
        connection.close()

        if user:
            raise ValidationError('That email is already owned by someone. Please choose a different one!')


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Login')


class AddToMyCartForm(FlaskForm):
    quantity_desired = IntegerField('Quantity', default=1, validators=[DataRequired('Please enter a number between 1 and 10!'), NumberRange(min=1, max=10)])
    add_to_my_cart = SubmitField('Add to my cart')


from manage import connect_to_mysql_db

